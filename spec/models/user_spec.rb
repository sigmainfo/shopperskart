require 'rails_helper'
require 'shoulda-matchers'

describe User, type: :model do
	it "has a valid for product" do
	FactoryGirl.create(:user).should be_valid
 end
before(:context) do
@user=FactoryGirl.build(:user)
	end	

	context "validation of user signup" do

		

		it { should validate_presence_of(:email) }

        it { should validate_presence_of(:password) }

		it { should validate_length_of(:password_confirmation) }

		
	end
	it "is invalid without a proper attributes" do
  	FactoryGirl.build(:user, email: nil, password: nil ,password_confirmation: nil).should_not be_valid
end
end

