Rails.application.routes.draw do
  resources :carts
  mount RailsAdmin::Engine => '/admin', as: 'rails_admin'

  resources :offlinestoredetails
  resources :offlinestores
  resources :onlinestores

  resources :categories
  resources :products
  resources :roles
    post 'products/addtocart'
root to: 'home#index'

devise_for :users



  # For details on the DSL available within this file, see http://guides.rubyonrails.org/routing.html
end
