class CreateOnlinestores < ActiveRecord::Migration[5.0]
  def change
    create_table :onlinestores do |t|
      t.integer :unit

      t.timestamps
    end
  end
end
