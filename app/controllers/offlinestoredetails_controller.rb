class OfflinestoredetailsController < ApplicationController
  before_action :set_offlinestoredetail, only: [:show, :edit, :update, :destroy]

  # GET /offlinestoredetails
  # GET /offlinestoredetails.json
  def index
    @offlinestoredetails = Offlinestoredetail.all
    @hash = Gmaps4rails.build_markers(@offlinestoredetails) do |user, marker|
    marker.lat user.latitude
    marker.lng user.longitude
    end
  end

  # GET /offlinestoredetails/1
  # GET /offlinestoredetails/1.json
  def show
  end

  # GET /offlinestoredetails/new
  def new
    @offlinestoredetail = Offlinestoredetail.new
  end

  # GET /offlinestoredetails/1/edit
  def edit
  end

  # POST /offlinestoredetails
  # POST /offlinestoredetails.json
  def create
    @offlinestoredetail = Offlinestoredetail.new(offlinestoredetail_params)

    respond_to do |format|
      if @offlinestoredetail.save
        format.html { redirect_to @offlinestoredetail, notice: 'Offlinestoredetail was successfully created.' }
        format.json { render :show, status: :created, location: @offlinestoredetail }
      else
        format.html { render :new }
        format.json { render json: @offlinestoredetail.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /offlinestoredetails/1
  # PATCH/PUT /offlinestoredetails/1.json
  def update
    respond_to do |format|
      if @offlinestoredetail.update(offlinestoredetail_params)
        format.html { redirect_to @offlinestoredetail, notice: 'Offlinestoredetail was successfully updated.' }
        format.json { render :show, status: :ok, location: @offlinestoredetail }
      else
        format.html { render :edit }
        format.json { render json: @offlinestoredetail.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /offlinestoredetails/1
  # DELETE /offlinestoredetails/1.json
  def destroy
    @offlinestoredetail.destroy
    respond_to do |format|
      format.html { redirect_to offlinestoredetails_url, notice: 'Offlinestoredetail was successfully destroyed.' }
      format.json { head :no_content }
    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_offlinestoredetail
      @offlinestoredetail = Offlinestoredetail.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def offlinestoredetail_params
      params.require(:offlinestoredetail).permit(:name, :address1, :address2, :city, :state, :country, :zip, :latitude, :longitude)
    end
end
