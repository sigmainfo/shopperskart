require "rails_helper"

RSpec.describe OnlinestoresController, type: :routing do
  describe "routing" do

    it "routes to #index" do
      expect(:get => "/onlinestores").to route_to("onlinestores#index")
    end

    it "routes to #new" do
      expect(:get => "/onlinestores/new").to route_to("onlinestores#new")
    end

    it "routes to #show" do
      expect(:get => "/onlinestores/1").to route_to("onlinestores#show", :id => "1")
    end

    it "routes to #edit" do
      expect(:get => "/onlinestores/1/edit").to route_to("onlinestores#edit", :id => "1")
    end

    it "routes to #create" do
      expect(:post => "/onlinestores").to route_to("onlinestores#create")
    end

    it "routes to #update via PUT" do
      expect(:put => "/onlinestores/1").to route_to("onlinestores#update", :id => "1")
    end

    it "routes to #update via PATCH" do
      expect(:patch => "/onlinestores/1").to route_to("onlinestores#update", :id => "1")
    end

    it "routes to #destroy" do
      expect(:delete => "/onlinestores/1").to route_to("onlinestores#destroy", :id => "1")
    end

  end
end
