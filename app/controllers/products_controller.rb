class ProductsController < ApplicationController
    before_action :active_add_to_cart!, only: [:addtocart]
  before_action :set_product, only: [:show, :edit, :update, :destroy]

  # GET /products
  # GET /products.json
  def index
    @products = Product.all
  end

  # GET /products/1
  # GET /products/1.json
  def show
  end

  # GET /products/new
  def new
    @product = Product.new
  end

  # GET /products/1/edit
  def edit
  end

  def addtocart
        @product = Product.find(params[:id])
      @cart = Cart.find_by(product_id: params[:id], user_id: session['user'])


      if @cart.nil?
        @cart = Cart.new(product_id: params[:id], user_id: session['user'])
        if @cart.save
          message = 'Added Product Successfully in your Cart'
        else
          message = 'Something wrong while Adding this Product in your Cart'
        end

      else
    message = 'You already Added this Product in your Cart'
  end
   respond_to do |format|
        format.html { redirect_to carts_path(id: @product.id), flash: { notice: message } }
        format.json { render :show, location: @product }
      end
    end








  # POST /products
  # POST /products.json
  def create
    @product = Product.new(product_params)

    respond_to do |format|
      if @product.save
        format.html { redirect_to @product, notice: 'Product was successfully created.' }
        format.json { render :show, status: :created, location: @product }
      else
        format.html { render :new }
        format.json { render json: @product.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /products/1
  # PATCH/PUT /products/1.json
  def update
    respond_to do |format|
      if @product.update(product_params)
        format.html { redirect_to @product, notice: 'Product was successfully updated.' }
        format.json { render :show, status: :ok, location: @product }
      else
        format.html { render :edit }
        format.json { render json: @product.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /products/1
  # DELETE /products/1.json
  def destroy
    @product.destroy
    respond_to do |format|
      format.html { redirect_to products_url, notice: 'Product was successfully destroyed.' }
      format.json { head :no_content }
    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_product
      @product = Product.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def product_params
      params.require(:product).permit(:title, :description, :price, :status, :shipping ,:pimage,:category_id)
    end
end
