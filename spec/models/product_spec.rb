require 'rails_helper'
require 'shoulda-matchers'

describe Product, type: :model do

	it "has a valid for product" do
	FactoryGirl.create(:product).should be_valid
 end

before(:context) do
		@product = FactoryGirl.build(:product)
	end

	context "validation of product" do


        it { should validate_presence_of(:title) }
        it { should validate_presence_of(:description) }
        it { should validate_presence_of(:price) }
        it { should validate_presence_of(:status ) }
				it { should validate_presence_of(:shipping ) }
		end
	it "is invalid without a proper attributes" do
  	FactoryGirl.build(:product, title: nil, description: nil ,price: nil ,status: nil,shipping: nil ).should_not be_valid
	end
end
