require 'rails_helper'

RSpec.describe "offlinestores/show", type: :view do
  before(:each) do
    @offlinestore = assign(:offlinestore, Offlinestore.create!(
      :unit => 2
    ))
  end

  it "renders attributes in <p>" do
    render
    expect(rendered).to match(/2/)
  end
end
