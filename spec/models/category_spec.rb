require 'rails_helper'
require 'shoulda-matchers'

describe Category, type: :model do

	it "has a valid for category" do
	FactoryGirl.create(:category).should be_valid
 end

 before(:context) do
		@product = FactoryGirl.build(:category)
	end

	context "validation of product" do


        it { should validate_presence_of(:title) }

		end
	it "is invalid without a proper field values" do
  	FactoryGirl.build(:category, title: nil ).should_not be_valid
	end
end
