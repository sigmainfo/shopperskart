FactoryGirl.define do
  factory :product do

    title {Faker::Commerce.product_name}
    description {Faker::Commerce.product_name}
    price {Faker::Commerce.price}
    status "yes"
    shipping "5"
  end
end
