require 'rails_helper'

RSpec.describe "offlinestoredetails/new", type: :view do
  before(:each) do
    assign(:offlinestoredetail, Offlinestoredetail.new(
      :name => "MyString",
      :address1 => "MyText",
      :address2 => "MyText",
      :city => "MyString",
      :state => "MyString",
      :country => "MyString",
      :zip => 1,
      :latitude => 1.5,
      :longitude => 1.5
    ))
  end

  it "renders new offlinestoredetail form" do
    render

    assert_select "form[action=?][method=?]", offlinestoredetails_path, "post" do

      assert_select "input#offlinestoredetail_name[name=?]", "offlinestoredetail[name]"

      assert_select "textarea#offlinestoredetail_address1[name=?]", "offlinestoredetail[address1]"

      assert_select "textarea#offlinestoredetail_address2[name=?]", "offlinestoredetail[address2]"

      assert_select "input#offlinestoredetail_city[name=?]", "offlinestoredetail[city]"

      assert_select "input#offlinestoredetail_state[name=?]", "offlinestoredetail[state]"

      assert_select "input#offlinestoredetail_country[name=?]", "offlinestoredetail[country]"

      assert_select "input#offlinestoredetail_zip[name=?]", "offlinestoredetail[zip]"

      assert_select "input#offlinestoredetail_latitude[name=?]", "offlinestoredetail[latitude]"

      assert_select "input#offlinestoredetail_longitude[name=?]", "offlinestoredetail[longitude]"
    end
  end
end
