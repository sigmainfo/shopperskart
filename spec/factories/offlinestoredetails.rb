FactoryGirl.define do
  factory :offlinestoredetail do
    name {Faker::Commerce.product_name}
    address1 {Faker::Address.street_address}
    address2 {Faker::Address.secondary_address}
    city {Faker::Address.city}
    state {Faker::Address.state}
    country {Faker::Address.country}
    zip 1
    latitude 1.5
    longitude 1.5
    trait :valid do
      name {Faker::Commerce.product_name}
      address1 {Faker::Address.street_address}
      address2 {Faker::Address.secondary_address}
      city {Faker::Address.city}
      state {Faker::Address.state}
      country {Faker::Address.country}
      zip 1
      latitude 1.5
      longitude 1.5
    end
    trait :invalid do
      name nil
      address1 nil
      address2 nil
      city nil
      state nil
      country nil
      zip nil
      latitude 1.5
      longitude 1.5
    end
    trait :newvalid do
      name {Faker::Commerce.product_name}
      address1 {Faker::Address.street_address}
      address2 {Faker::Address.secondary_address}
      city {Faker::Address.city}
      state {Faker::Address.state}
      country {Faker::Address.country}
      zip 2
      latitude 1.5
      longitude 1.5
    end
  end
end
