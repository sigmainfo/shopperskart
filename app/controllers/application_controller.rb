class ApplicationController < ActionController::Base
  protect_from_forgery with: :exception
  rescue_from CanCan::AccessDenied do |exception|
  redirect_to main_app.root_path, :alert => exception.message
end

def active_add_to_cart!
   if current_user.nil?
        redirect_to new_user_session_path,  alert: 'You need to login before add to cart'
  end
  return true
end


end
