class AddAttachmentPimageToProducts < ActiveRecord::Migration
  def self.up
    change_table :products do |t|
      t.attachment :pimage
    end
  end

  def self.down
    remove_attachment :products, :pimage
  end
end
