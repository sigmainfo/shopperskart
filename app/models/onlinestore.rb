class Onlinestore < ApplicationRecord
    has_many :products, dependent: :destroy
    validates_presence_of :unit
end
