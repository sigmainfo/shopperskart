json.extract! onlinestore, :id, :unit, :created_at, :updated_at
json.url onlinestore_url(onlinestore, format: :json)