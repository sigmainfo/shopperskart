require 'rails_helper'
require 'shoulda-matchers'

describe Offlinestore, type: :model do

	it "has a valid for Offlinestore" do
	FactoryGirl.create(:offlinestore).should be_valid
 end

 before(:context) do
		@product = FactoryGirl.build(:offlinestore)
	end

	context "validation of offlinestore" do


        it { should validate_presence_of(:unit) }

		end
	it "is invalid without a proper field values" do
  	FactoryGirl.build(:offlinestore, unit: nil ).should_not be_valid
	end
end
