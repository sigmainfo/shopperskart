class CreateOfflinestores < ActiveRecord::Migration[5.0]
  def change
    create_table :offlinestores do |t|
      t.integer :unit

      t.timestamps
    end
  end
end
