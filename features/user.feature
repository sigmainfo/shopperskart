
Feature: users signin
  As a user
  I want to be able to login

Scenario: users signup(successful)
Given As a user I should see home page
When I click on signup button


Scenario: users signup(without email)
Given As a user I need to check signup without email


Scenario: users signup(mismatched password)
Given As a user I need to check signup with mismatched password

Scenario: users signup(with already registered email)
Given As a user I need to check signup with already registered email

Scenario: users signin(successful)
When I click on login button

 Scenario: users signin(invalid email)
 Given As a user I need to check login with invalid email

 Scenario: users signin(email blank)
 Given As a user I need to check login with email blank

 Scenario: users signin(password blank)
 As a user I need to check login without email and password

 Scenario:Forgot password(successful)
 Given As a user I need to check forgot password link

 Scenario:Forgot password(email not found)
 Given As a user I need to check forgot password when email not found

 Scenario:Forgot password(email blank)
 Given As a user I need to check forgot password when email blank