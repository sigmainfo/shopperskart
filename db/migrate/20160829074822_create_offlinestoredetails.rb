class CreateOfflinestoredetails < ActiveRecord::Migration[5.0]
  def change
    create_table :offlinestoredetails do |t|
      t.string :name
      t.text :address1
      t.text :address2
      t.string :city
      t.string :state
      t.string :country
      t.integer :zip
      t.float :latitude
      t.float :longitude

      t.timestamps
    end
  end
end
