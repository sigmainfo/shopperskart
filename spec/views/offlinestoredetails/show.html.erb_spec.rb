require 'rails_helper'

RSpec.describe "offlinestoredetails/show", type: :view do
  before(:each) do
    @offlinestoredetail = assign(:offlinestoredetail, Offlinestoredetail.create!(
      :name => "Name",
      :address1 => "MyText",
      :address2 => "MyText",
      :city => "City",
      :state => "State",
      :country => "Country",
      :zip => 2,
      :latitude => 3.5,
      :longitude => 4.5
    ))
  end

  it "renders attributes in <p>" do
    render
    expect(rendered).to match(/Name/)
    expect(rendered).to match(/MyText/)
    expect(rendered).to match(/MyText/)
    expect(rendered).to match(/City/)
    expect(rendered).to match(/State/)
    expect(rendered).to match(/Country/)
    expect(rendered).to match(/2/)
    expect(rendered).to match(/3.5/)
    expect(rendered).to match(/4.5/)
  end
end
