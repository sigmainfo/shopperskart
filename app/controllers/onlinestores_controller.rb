class OnlinestoresController < ApplicationController
  before_action :set_onlinestore, only: [:show, :edit, :update, :destroy]

  # GET /onlinestores
  # GET /onlinestores.json
  def index
    @onlinestores = Onlinestore.all
  end

  # GET /onlinestores/1
  # GET /onlinestores/1.json
  def show
  end

  # GET /onlinestores/new
  def new
    @onlinestore = Onlinestore.new
  end

  # GET /onlinestores/1/edit
  def edit
  end

  # POST /onlinestores
  # POST /onlinestores.json
  def create
    @onlinestore = Onlinestore.new(onlinestore_params)

    respond_to do |format|
      if @onlinestore.save
        format.html { redirect_to @onlinestore, notice: 'Onlinestore was successfully created.' }
        format.json { render :show, status: :created, location: @onlinestore }
      else
        format.html { render :new }
        format.json { render json: @onlinestore.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /onlinestores/1
  # PATCH/PUT /onlinestores/1.json
  def update
    respond_to do |format|
      if @onlinestore.update(onlinestore_params)
        format.html { redirect_to @onlinestore, notice: 'Onlinestore was successfully updated.' }
        format.json { render :show, status: :ok, location: @onlinestore }
      else
        format.html { render :edit }
        format.json { render json: @onlinestore.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /onlinestores/1
  # DELETE /onlinestores/1.json
  def destroy
    @onlinestore.destroy
    respond_to do |format|
      format.html { redirect_to onlinestores_url, notice: 'Onlinestore was successfully destroyed.' }
      format.json { head :no_content }
    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_onlinestore
      @onlinestore = Onlinestore.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def onlinestore_params
      params.require(:onlinestore).permit(:unit)
    end
end
