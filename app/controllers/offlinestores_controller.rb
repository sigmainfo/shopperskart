class OfflinestoresController < ApplicationController
  before_action :set_offlinestore, only: [:show, :edit, :update, :destroy]

  # GET /offlinestores
  # GET /offlinestores.json
  def index
    @offlinestores = Offlinestore.all
  end

  # GET /offlinestores/1
  # GET /offlinestores/1.json
  def show
  end

  # GET /offlinestores/new
  def new
    @offlinestore = Offlinestore.new
  end

  # GET /offlinestores/1/edit
  def edit
  end

  # POST /offlinestores
  # POST /offlinestores.json
  def create
    @offlinestore = Offlinestore.new(offlinestore_params)

    respond_to do |format|
      if @offlinestore.save
        format.html { redirect_to @offlinestore, notice: 'Offlinestore was successfully created.' }
        format.json { render :show, status: :created, location: @offlinestore }
      else
        format.html { render :new }
        format.json { render json: @offlinestore.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /offlinestores/1
  # PATCH/PUT /offlinestores/1.json
  def update
    respond_to do |format|
      if @offlinestore.update(offlinestore_params)
        format.html { redirect_to @offlinestore, notice: 'Offlinestore was successfully updated.' }
        format.json { render :show, status: :ok, location: @offlinestore }
      else
        format.html { render :edit }
        format.json { render json: @offlinestore.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /offlinestores/1
  # DELETE /offlinestores/1.json
  def destroy
    @offlinestore.destroy
    respond_to do |format|
      format.html { redirect_to offlinestores_url, notice: 'Offlinestore was successfully destroyed.' }
      format.json { head :no_content }
    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_offlinestore
      @offlinestore = Offlinestore.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def offlinestore_params
      params.require(:offlinestore).permit(:unit)
    end
end
