require 'rails_helper'

RSpec.describe "onlinestores/show", type: :view do
  before(:each) do
    @onlinestore = assign(:onlinestore, Onlinestore.create!(
      :unit => 2
    ))
  end

  it "renders attributes in <p>" do
    render
    expect(rendered).to match(/2/)
  end
end
