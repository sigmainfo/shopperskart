require 'rails_helper'

RSpec.describe "offlinestores/index", type: :view do
  before(:each) do
    assign(:offlinestores, [
      Offlinestore.create!(
        :unit => 2
      ),
      Offlinestore.create!(
        :unit => 2
      )
    ])
  end

  it "renders a list of offlinestores" do
    render
    assert_select "tr>td", :text => 2.to_s, :count => 2
  end
end
