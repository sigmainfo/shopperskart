require 'rails_helper'

RSpec.describe "offlinestores/edit", type: :view do
  before(:each) do
    @offlinestore = assign(:offlinestore, Offlinestore.create!(
      :unit => 1
    ))
  end

  it "renders the edit offlinestore form" do
    render

    assert_select "form[action=?][method=?]", offlinestore_path(@offlinestore), "post" do

      assert_select "input#offlinestore_unit[name=?]", "offlinestore[unit]"
    end
  end
end
