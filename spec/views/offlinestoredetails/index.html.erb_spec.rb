require 'rails_helper'

RSpec.describe "offlinestoredetails/index", type: :view do
  before(:each) do
    assign(:offlinestoredetails, [
      Offlinestoredetail.create!(
        :name => "Name",
        :address1 => "MyText",
        :address2 => "MyText",
        :city => "City",
        :state => "State",
        :country => "Country",
        :zip => 2,
        :latitude => 3.5,
        :longitude => 4.5
      ),
      Offlinestoredetail.create!(
        :name => "Name",
        :address1 => "MyText",
        :address2 => "MyText",
        :city => "City",
        :state => "State",
        :country => "Country",
        :zip => 2,
        :latitude => 3.5,
        :longitude => 4.5
      )
    ])
  end

  it "renders a list of offlinestoredetails" do
    render
    assert_select "tr>td", :text => "Name".to_s, :count => 2
    assert_select "tr>td", :text => "MyText".to_s, :count => 2
    assert_select "tr>td", :text => "MyText".to_s, :count => 2
    assert_select "tr>td", :text => "City".to_s, :count => 2
    assert_select "tr>td", :text => "State".to_s, :count => 2
    assert_select "tr>td", :text => "Country".to_s, :count => 2
    assert_select "tr>td", :text => 2.to_s, :count => 2
    assert_select "tr>td", :text => 3.5.to_s, :count => 2
    assert_select "tr>td", :text => 4.5.to_s, :count => 2
  end
end
