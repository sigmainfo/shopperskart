class Product < ApplicationRecord

    has_attached_file :pimage, :styles => { :medium => "200x200>", :thumb => "50x50>" }, :default_url => "medium/default_image.png"
    validates_attachment_content_type :pimage, content_type: /\Aimage\/.*\z/
    validates_presence_of :title, :description, :price, :status, :shipping
    belongs_to :category
    has_many :offlinestoredetails, :through => :offlinestores

end
