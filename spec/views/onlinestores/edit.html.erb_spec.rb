require 'rails_helper'

RSpec.describe "onlinestores/edit", type: :view do
  before(:each) do
    @onlinestore = assign(:onlinestore, Onlinestore.create!(
      :unit => 1
    ))
  end

  it "renders the edit onlinestore form" do
    render

    assert_select "form[action=?][method=?]", onlinestore_path(@onlinestore), "post" do

      assert_select "input#onlinestore_unit[name=?]", "onlinestore[unit]"
    end
  end
end
