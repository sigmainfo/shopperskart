require 'rails_helper'

# This spec was generated by rspec-rails when you ran the scaffold generator.
# It demonstrates how one might use RSpec to specify the controller code that
# was generated by Rails when you ran the scaffold generator.
#
# It assumes that the implementation code is generated by the rails scaffold
# generator.  If you are using any extension libraries to generate different
# controller code, this generated spec may or may not pass.
#
# It only uses APIs available in rails and/or rspec-rails.  There are a number
# of tools you can use to make these specs even more expressive, but we're
# sticking to rails and rspec-rails APIs to keep things simple and stable.
#
# Compared to earlier versions of this generator, there is very limited use of
# stubs and message expectations in this spec.  Stubs are only used when there
# is no simpler way to get a handle on the object needed for the example.
# Message expectations are only used when there is no simpler way to specify
# that an instance is receiving a specific message.

RSpec.describe OfflinestoredetailsController, type: :controller do

  # This should return the minimal set of attributes required to create a valid
  # Offlinestoredetail. As you add validations to Offlinestoredetail, be sure to
  # adjust the attributes here as well.
  let(:valid_attributes) {
    #skip("Add a hash of attributes valid for your model")
    FactoryGirl.attributes_for :offlinestoredetail, :valid
  }

  let(:invalid_attributes) {
    #skip("Add a hash of attributes invalid for your model")
    FactoryGirl.attributes_for :offlinestoredetail, :invalid
  }

  # This should return the minimal set of values that should be in the session
  # in order to pass any filters (e.g. authentication) defined in
  # OfflinestoredetailsController. Be sure to keep this updated too.
  let(:valid_session) { {} }

  describe "GET #index" do
    it "assigns all offlinestoredetails as @offlinestoredetails" do
      offlinestoredetail = Offlinestoredetail.create! valid_attributes
      get :index, params: {}, session: valid_session
      expect(assigns(:offlinestoredetails)).to eq([offlinestoredetail])
    end
  end

  describe "GET #show" do
    it "assigns the requested offlinestoredetail as @offlinestoredetail" do
      offlinestoredetail = Offlinestoredetail.create! valid_attributes
      get :show, params: {id: offlinestoredetail.to_param}, session: valid_session
      expect(assigns(:offlinestoredetail)).to eq(offlinestoredetail)
    end
  end

  describe "GET #new" do
    it "assigns a new offlinestoredetail as @offlinestoredetail" do
      get :new, params: {}, session: valid_session
      expect(assigns(:offlinestoredetail)).to be_a_new(Offlinestoredetail)
    end
  end

  describe "GET #edit" do
    it "assigns the requested offlinestoredetail as @offlinestoredetail" do
      offlinestoredetail = Offlinestoredetail.create! valid_attributes
      get :edit, params: {id: offlinestoredetail.to_param}, session: valid_session
      expect(assigns(:offlinestoredetail)).to eq(offlinestoredetail)
    end
  end

  describe "POST #create" do
    context "with valid params" do
      it "creates a new Offlinestoredetail" do
        expect {
          post :create, params: {offlinestoredetail: valid_attributes}, session: valid_session
        }.to change(Offlinestoredetail, :count).by(1)
      end

      it "assigns a newly created offlinestoredetail as @offlinestoredetail" do
        post :create, params: {offlinestoredetail: valid_attributes}, session: valid_session
        expect(assigns(:offlinestoredetail)).to be_a(Offlinestoredetail)
        expect(assigns(:offlinestoredetail)).to be_persisted
      end

      it "redirects to the created offlinestoredetail" do
        post :create, params: {offlinestoredetail: valid_attributes}, session: valid_session
        expect(response).to redirect_to(Offlinestoredetail.last)
      end
    end

    context "with invalid params" do
      it "assigns a newly created but unsaved offlinestoredetail as @offlinestoredetail" do
        post :create, params: {offlinestoredetail: invalid_attributes}, session: valid_session
        expect(assigns(:offlinestoredetail)).to be_a_new(Offlinestoredetail)
      end

      it "re-renders the 'new' template" do
        post :create, params: {offlinestoredetail: invalid_attributes}, session: valid_session
        expect(response).to render_template("new")
      end
    end
  end

  describe "PUT #update" do
    context "with valid params" do
      let(:new_attributes) {
        #skip("Add a hash of attributes valid for your model")
        FactoryGirl.attributes_for :offlinestoredetail, :newvalid
      }

      it "updates the requested offlinestoredetail" do
        offlinestoredetail = Offlinestoredetail.create! valid_attributes
        put :update, params: {id: offlinestoredetail.to_param, offlinestoredetail: new_attributes}, session: valid_session
        offlinestoredetail.reload
        expect(assigns(:offlinestoredetail)).to eq(offlinestoredetail)
        #skip("Add assertions for updated state")
      end

      it "assigns the requested offlinestoredetail as @offlinestoredetail" do
        offlinestoredetail = Offlinestoredetail.create! valid_attributes
        put :update, params: {id: offlinestoredetail.to_param, offlinestoredetail: valid_attributes}, session: valid_session
        expect(assigns(:offlinestoredetail)).to eq(offlinestoredetail)
      end

      it "redirects to the offlinestoredetail" do
        offlinestoredetail = Offlinestoredetail.create! valid_attributes
        put :update, params: {id: offlinestoredetail.to_param, offlinestoredetail: valid_attributes}, session: valid_session
        expect(response).to redirect_to(offlinestoredetail)
      end
    end

    context "with invalid params" do
      it "assigns the offlinestoredetail as @offlinestoredetail" do
        offlinestoredetail = Offlinestoredetail.create! valid_attributes
        put :update, params: {id: offlinestoredetail.to_param, offlinestoredetail: invalid_attributes}, session: valid_session
        expect(assigns(:offlinestoredetail)).to eq(offlinestoredetail)
      end

      it "re-renders the 'edit' template" do
        offlinestoredetail = Offlinestoredetail.create! valid_attributes
        put :update, params: {id: offlinestoredetail.to_param, offlinestoredetail: invalid_attributes}, session: valid_session
        expect(response).to render_template("edit")
      end
    end
  end

  describe "DELETE #destroy" do
    it "destroys the requested offlinestoredetail" do
      offlinestoredetail = Offlinestoredetail.create! valid_attributes
      expect {
        delete :destroy, params: {id: offlinestoredetail.to_param}, session: valid_session
      }.to change(Offlinestoredetail, :count).by(-1)
    end

    it "redirects to the offlinestoredetails list" do
      offlinestoredetail = Offlinestoredetail.create! valid_attributes
      delete :destroy, params: {id: offlinestoredetail.to_param}, session: valid_session
      expect(response).to redirect_to(offlinestoredetails_url)
    end
  end
  describe "Dependent DELETE #destroy" do
    it "destroys the requested offlinestoredetail" do
      offlinestoredetail = Offlinestoredetail.create! valid_attributes
      expect {
        delete :destroy, params: {id: offlinestoredetail.to_param}, session: valid_session
      }.to change(Offlinestoredetail, :count).by(-1)
    end

    it "redirects to the offlinestoredetails list" do
      offlinestoredetail = Offlinestoredetail.create! valid_attributes
      delete :destroy, params: {id: offlinestoredetail.to_param}, session: valid_session
      expect(response).to redirect_to(offlinestoredetails_url)
    end
  end

end
