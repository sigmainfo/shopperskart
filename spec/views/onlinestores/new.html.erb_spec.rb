require 'rails_helper'

RSpec.describe "onlinestores/new", type: :view do
  before(:each) do
    assign(:onlinestore, Onlinestore.new(
      :unit => 1
    ))
  end

  it "renders new onlinestore form" do
    render

    assert_select "form[action=?][method=?]", onlinestores_path, "post" do

      assert_select "input#onlinestore_unit[name=?]", "onlinestore[unit]"
    end
  end
end
