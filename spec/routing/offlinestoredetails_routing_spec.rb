require "rails_helper"

RSpec.describe OfflinestoredetailsController, type: :routing do
  describe "routing" do

    it "routes to #index" do
      expect(:get => "/offlinestoredetails").to route_to("offlinestoredetails#index")
    end

    it "routes to #new" do
      expect(:get => "/offlinestoredetails/new").to route_to("offlinestoredetails#new")
    end

    it "routes to #show" do
      expect(:get => "/offlinestoredetails/1").to route_to("offlinestoredetails#show", :id => "1")
    end

    it "routes to #edit" do
      expect(:get => "/offlinestoredetails/1/edit").to route_to("offlinestoredetails#edit", :id => "1")
    end

    it "routes to #create" do
      expect(:post => "/offlinestoredetails").to route_to("offlinestoredetails#create")
    end

    it "routes to #update via PUT" do
      expect(:put => "/offlinestoredetails/1").to route_to("offlinestoredetails#update", :id => "1")
    end

    it "routes to #update via PATCH" do
      expect(:patch => "/offlinestoredetails/1").to route_to("offlinestoredetails#update", :id => "1")
    end

    it "routes to #destroy" do
      expect(:delete => "/offlinestoredetails/1").to route_to("offlinestoredetails#destroy", :id => "1")
    end

  end
end
