require 'rails_helper'
require 'shoulda-matchers'

describe Offlinestoredetail, type: :model do

	it "has a valid for offlinestoredetail" do
	FactoryGirl.create(:offlinestoredetail).should be_valid
 end

 before(:context) do
		@product = FactoryGirl.build(:offlinestoredetail)
	end

	context "validation of offlinestoredetail" do


        it { should validate_presence_of(:name) }
        it { should validate_presence_of(:address1) }
        it { should validate_presence_of(:address2) }
        it { should validate_presence_of(:city) }
        it { should validate_presence_of(:state) }
        it { should validate_presence_of(:country) }
        it { should validate_presence_of(:zip) }

		end
	it "is invalid without a proper field values" do
  	FactoryGirl.build(:offlinestoredetail, name:nil, address1:nil, address2:nil, city:nil, state:nil, country:nil, zip:nil ).should_not be_valid
	end
end
