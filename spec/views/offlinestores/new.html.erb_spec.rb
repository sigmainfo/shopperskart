require 'rails_helper'

RSpec.describe "offlinestores/new", type: :view do
  before(:each) do
    assign(:offlinestore, Offlinestore.new(
      :unit => 1
    ))
  end

  it "renders new offlinestore form" do
    render

    assert_select "form[action=?][method=?]", offlinestores_path, "post" do

      assert_select "input#offlinestore_unit[name=?]", "offlinestore[unit]"
    end
  end
end
