class Offlinestoredetail < ApplicationRecord
  has_many :products, :through => :offlinestores
  geocoded_by :address
  after_validation :geocode
  validates_presence_of :name, :address1, :address2, :city, :state, :country, :zip

  def address
  str=""
  if self.zip==nil
    str += self.address1 if self.address1!=nil
    str += ', ' if str != nil && self.address1!=nil
    str += self.address2 if self.address2!=nil
    str += ', ' if str != nil && self.address2!=nil
    str += self.city if self.city!=nil
    str += ', ' if str != nil && self.city!=nil
    str += self.state if self.state!=nil
    str += ', ' if str != nil && self.state!=nil
    str += self.country if self.country!=nil
    str += ', ' if str != nil && self.country!=nil
  else
    str=self.zip
  end
  str
end

end
