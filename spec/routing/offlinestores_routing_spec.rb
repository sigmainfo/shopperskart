require "rails_helper"

RSpec.describe OfflinestoresController, type: :routing do
  describe "routing" do

    it "routes to #index" do
      expect(:get => "/offlinestores").to route_to("offlinestores#index")
    end

    it "routes to #new" do
      expect(:get => "/offlinestores/new").to route_to("offlinestores#new")
    end

    it "routes to #show" do
      expect(:get => "/offlinestores/1").to route_to("offlinestores#show", :id => "1")
    end

    it "routes to #edit" do
      expect(:get => "/offlinestores/1/edit").to route_to("offlinestores#edit", :id => "1")
    end

    it "routes to #create" do
      expect(:post => "/offlinestores").to route_to("offlinestores#create")
    end

    it "routes to #update via PUT" do
      expect(:put => "/offlinestores/1").to route_to("offlinestores#update", :id => "1")
    end

    it "routes to #update via PATCH" do
      expect(:patch => "/offlinestores/1").to route_to("offlinestores#update", :id => "1")
    end

    it "routes to #destroy" do
      expect(:delete => "/offlinestores/1").to route_to("offlinestores#destroy", :id => "1")
    end

  end
end
