json.extract! offlinestore, :id, :unit, :created_at, :updated_at
json.url offlinestore_url(offlinestore, format: :json)