require 'rails_helper'
require 'shoulda-matchers'

describe Onlinestore, type: :model do

	it "has a valid for Onlinestore" do
	FactoryGirl.create(:onlinestore).should be_valid
 end

 before(:context) do
		@product = FactoryGirl.build(:onlinestore)
	end

	context "validation of onlinestore" do


        it { should validate_presence_of(:unit) }

		end
	it "is invalid without a proper field values" do
  	FactoryGirl.build(:onlinestore, unit: nil ).should_not be_valid
	end
end
