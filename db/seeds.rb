# This file should contain all the record creation needed to seed the database with its default values.
# The data can then be loaded with the rails db:seed command (or created alongside the database with db:setup).
#
# Examples:
#
#   movies = Movie.create([{ name: 'Star Wars' }, { name: 'Lord of the Rings' }])
#   Character.create(name: 'Luke', movie: movies.first)

if !Role.exists?
  admin_role=Role.create authority:'admin'
end

if !User.find_by(email: 'admin@gmail.com').present?
admin = User.new(
    :email => 'admin@gmail.com',
    :password => 'sigma@123',
    :password_confirmation => 'sigma@123',
    :role_id=>'1',
    :name =>'admin'
  )

admin.save!
end
