require 'rails_helper'

RSpec.describe "onlinestores/index", type: :view do
  before(:each) do
    assign(:onlinestores, [
      Onlinestore.create!(
        :unit => 2
      ),
      Onlinestore.create!(
        :unit => 2
      )
    ])
  end

  it "renders a list of onlinestores" do
    render
    assert_select "tr>td", :text => 2.to_s, :count => 2
  end
end
