Given(/^As a user I should see home page$/) do
  root_path
end
When (/^I click on signup button$/)  do
visit "http://localhost:3000"
click_link('Sign up')
fill_in('user_email', :with => 'devi@gmail.com')
fill_in('user_password', :with => 'password')
fill_in('user_password_confirmation', :with => 'password')
click_button 'Sign up'
expect(page).to have_content("Welcome! You have signed up successfully.")
end

Given(/^As a user I need to check signup without email$/) do
visit "http://localhost:3000"
click_link('Sign up')
fill_in('user_email', :with => '')
fill_in('user_password', :with => 'password')
fill_in('user_password_confirmation', :with => 'password')
click_button 'Sign up'
expect(page).to have_content("Email can't be blank")
end

Given(/^As a user I need to check signup with mismatched password$/) do
visit "http://localhost:3000"
click_link('Sign up')
fill_in('user_email', :with => 'raj@gmail.com')
fill_in('user_password', :with => 'password')
fill_in('user_password_confirmation', :with => 'password12')
click_button 'Sign up'
expect(page).to have_content("Password confirmation doesn't match Password")
end

Given(/^As a user I need to check signup with already registered email$/) do
visit "http://localhost:3000"
click_link('Sign up')
User.new(:email => "raj@gmail.com", :password => 'password', :password_confirmation => 'password').save!
fill_in('user_email', :with => 'raj@gmail.com')
fill_in('user_password', :with => 'password')
fill_in('user_password_confirmation', :with => 'password')
click_button 'Sign up'
expect(page).to have_content("Email has already been taken")
end


When (/^I click on login button$/)  do
visit "http://localhost:3000"
click_link('Login')
User.new(:email => "raj@gmail.com", :password => 'password', :password_confirmation => 'password').save!
fill_in('user_email', :with => 'raj@gmail.com')
fill_in('user_password', :with => 'password')
click_button 'Log in'
expect(page).to have_content("Signed in successfully.")
end


Given(/^As a user I need to check forgot password link$/) do
visit("http://localhost:3000/")
click_link('Login')
User.new(:email => "raj@gmail.com", :password => 'password', :password_confirmation => 'password').save!
visit new_user_session_path
click_link('Forgot your password?')
visit new_user_password_path
fill_in('user_email', :with => 'raj12@gmail.com')
click_button 'Send me reset password instructions'
#expect(page).to have_content("You will receive an email with instructions on how to reset your password in a few minutes.")
end

Given(/^As a user I need to check login with invalid email$/) do
visit("http://localhost:3000/")
click_link('Login')
fill_in('user_email', :with => 'raj@gmail.com')
fill_in('user_password', :with => 'password')
click_button 'Log in'
expect(page).to have_content("Invalid Email or password.")
end

Given(/^As a user I need to check login with email blank$/) do
visit("http://localhost:3000/")
click_link('Login')
fill_in('user_email', :with => '')
fill_in('user_password', :with => 'password')
click_button 'Log in'
expect(page).to have_content("Invalid Email or password.")
end


Given(/^As a user I need to check login without email and password$/) do
visit("http://localhost:3000/")
click_link('Login')
fill_in('user_email', :with => '')
fill_in('user_password', :with => '')
click_button 'Log in'
expect(page).to have_content("Invalid Email or password.")
end



Given(/^As a user I need to check forgot password when email not found$/) do
visit("http://localhost:3000/")
click_link('Login')
visit new_user_session_path
click_link('Forgot your password?')
visit new_user_password_path
fill_in('user_email', :with => 'raj12@gmail.com')
click_button 'Send me reset password instructions'
expect(page).to have_content("Email not found")
end


Given(/^As a user I need to check forgot password when email blank$/) do
visit("http://localhost:3000/")
click_link('Login')

visit new_user_session_path
click_link('Forgot your password?')
visit new_user_password_path
fill_in('user_email', :with => '')
click_button 'Send me reset password instructions'
expect(page).to have_content("Email can't be blank")
end