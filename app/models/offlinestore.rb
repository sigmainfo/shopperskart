class Offlinestore < ApplicationRecord
  validates_presence_of :unit
  belongs_to :product
  belongs_to :offlinestore
end
